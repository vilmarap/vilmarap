# Hi there 👋, I'm Vilmara! =]

[![Github Badge](https://img.shields.io/badge/-Github-000?style=flat-square&logo=Github&logoColor=white&link=https://github.com/vilmarasilva)](https://github.com/vilmarasilva)
[![Linkedin Badge](https://img.shields.io/badge/-LinkedIn-blue?style=flat-square&logo=Linkedin&logoColor=white&link=https://www.linkedin.com/in/vilmarapereiras/)](https://www.linkedin.com/in/vilmarapereiras/)


### About me 💬

- Infrastructure | SRE :wrench:
- Information Technology Management and Cloud Computing :mortar_board:
- I've been working with some tools like: Docker | Terraform | Ansible | Linux | Windows Server | Datadog | New Relic | Backstage etc.. 🌱
- Clouds: Azure | AWS

- [Website](https://pessoalda-ti.blogspot.com/) 💻 - Working on it.

<div align="center">
  <a href="https://github.com/vilmarap">
  <img height="150em" src="https://github-readme-stats.vercel.app/api?username=vilmarap&show_icons=true&theme=dark&include_all_commits=true&count_private=true"/>
  <img height="150em" src="https://github-readme-stats.vercel.app/api/top-langs/?username=vilmarap&layout=compact&langs_count=7&theme=dark"/>
</div>
